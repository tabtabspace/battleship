# Software architecture for SWeng assignment #

* Author: Thomas Keevers
* Date: 27/11/2017
* V0.1

### Program outline ###
This is an outline for the Battleship game. 

### Program flow ###
* Load the ship fleet file
* Load the ship placements for player 1
* Load the ship placements for player 2
* Load the moves for both players
* If moves are exhausted, report World Peace has been achieved. 

### Classes ###
Fleet – This class keeps track of the ships that each player needs to lay down

	.name – The type of ship (E.g. Carrier)
	
	.length – The length of the ship (E.g. 2)
	
	.number – The number of each ship available
	
 	.player1 – An integer that keeps track of how many ships player 1 has laid down.
	
	.player2 – An integer that keeps track of how many ships player 2 has laid down.
	

Grid – There is one grid object for each player. It keeps track of the ship positions, how many times each ship has been hit, and the hits and misses that have been recorded. 
	
	.information – An NxN array filled with 0 (unknown), 1 (hit) or 2 (miss) that records what information is known to the opponent.
	
	.positions – An NxN array in which each entry specifies which ship occupies that position (or if no ship is present)
	
	.shipname – Keeps track of the link between the index given in .positions and what that ship is called (e.g. Carrier).
	
	.shiphealth – Keeps track of the number of hits required to destroy the ship. 

### Miscellaneous ###
* These are some additional design features that weren’t specified sufficiently in the requirements document:
* We will assume the grid is fixed at 10x10 for each player.
* We will assume that the grid is zero-indexed, from 0,0 to 9,9.
* Assume that if players exhaust their move list that the game ends in a draw. 