import unittest
import Fleet as flt
import Grid as grd
import PlayerAction as pa

class BattleshipTest(unittest.TestCase):

    def setUp(self):
        #not sure what goes in here
        fleet = flt.Fleet()
		
    def test_ShipLength(self):
        #self.assertEqual('Ship must have a positive length',fleet.ReadCSV('fleet_testshiplengthneg.csv')
        with self.assertRaises(Exception) as context:
            fleet.ReadCSV('/testcases/fleet_testshiplengthneg.csv')
            self.assertTrue('Ship must have a positive length' in context.exception)
			
    def test_ShipLength(self):
        #self.assertEqual('Ship must have a positive length',fleet.ReadCSV('fleet_testshiplengthneg.csv')
        with self.assertRaises(Exception) as context:
            fleet.ReadCSV('/testcases/fleet_testshiplengthpos.csv')
            self.assertTrue('Ship must have a positive length' in context.exception)
    	
if __name__ == '__main__':
    unittest.main()
