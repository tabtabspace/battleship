#This part is just me testing the file
import csv

# This part is the Fleet class
class Fleet:
    """Fleet class. Captures what ships each player must lay down."""
    
    def __init__(self):
        self.name = [] # Name for each type of ship
        self.number = [] # How many of each ship need to be plaed
        self.length = [] # The length of each ship
        self.player1 = [] # How many of each ship player 1 has placed
        self.player2 = [] # How many of each ship player 2 has placed
        
    def ReadCSV(self, filename):
        with open(filename, newline='') as csvfile:
            csvread = csv.reader(csvfile, delimiter=',', quotechar='|')
            next(csvread, None)  # skip the headers
            for row in csvread:
                name = row[0]
                if name in self.name:
                    raise Exception("Ship type has already been specified")
                self.name.append(name)
                
                number = int(row[1])
                if number < 1:
                    raise Exception("Must have at least one of each type of ship") 
            
                self.number.append(number)
                length = int(row[2])
                if length < 1:
                    raise Exception("Ship must have a positive length")                
                self.length.append(length)
                
        # Initialize ships for each player 
        self.player1 = [0]*len(self.name)
        self.player2 = [0]*len(self.name)
