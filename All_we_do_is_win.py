"""Run the battleship program
input four csvs:
Define the fleet
Player1 positions
Player2 positions
Moves file
Give these as text files like
'Marc_Wins.csv'
"""

def Do_Battleship(fleet_filename, player1_pos_filename, player2_pos_filename, moves_filename):
    
    import Fleet as flt
    import Grid as grd
    import PlayerAction as pa
    
    fleet = flt.Fleet()
    fleet.ReadCSV(fleet_filename)
    
    grid1 = grd.Grid()
    grid1.ReadShipPositions(player1_pos_filename, fleet)
    
    grid2 = grd.Grid()
    grid2.ReadShipPositions(player2_pos_filename, fleet)

    player = pa.PlayerAction()
    player.ReadMoves(moves_filename, grid1, grid2)
    
    grid1.printing()
    grid2.printing()
    
    grid1.PlotGrid()
    grid2.PlotGrid()
