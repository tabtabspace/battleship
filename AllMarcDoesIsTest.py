import unittest
import All_we_do_is_win
import Ship
# Here's our "unit tests".

class atest(unittest.TestCase):

    def testOne(self):
        fleet_filename = 'Fleet.csv'
        player1_pos_filename = 'Layout.csv'
        player2_pos_filename = 'Layout.csv'
        moves_filename = 'Moves.csv'
        self.failIf(All_we_do_is_win.Do_Battleship(fleet_filename, player1_pos_filename, player2_pos_filename, moves_filename))

    def testTwo(self):
        self.assertTrue(Ship.Ship(0, 0, 'Cow', 'H'))
  
    # Check that you cant place a ship diagonally
    def testThree(self):
        self.assertRaises(Exception , Ship.Ship,0, 0, 'Cow', 'D')
        
    # Check that you cant place a ship at negative locations
    def testRae(self):
        self.assertRaises(Exception , Ship.Ship, -1, -1, 'Cow', 'V')
        
   # Check that you cant place a ship at negative locations
    def testRoger(self):
        self.assertTrue(Ship.Ship(1, 1, 'Destroyer', 'V'))
        
    # Test you can sink all the ships (and win)
    def testSteve(self):
        import Grid
        grid = Grid.Grid()
        grid.shiphealth.append(1)
        grid.positions[0,0] = 0
        self.assertEqual(3, grid.HitShip(0, 0))

    # Test you can sink a the ships (and not win)
    def testPaul(self):
        import Grid
        grid = Grid.Grid()
        grid.shiphealth.append(1)
        grid.shiphealth.append(1)
        grid.positions[0,0] = 0
        self.assertEqual(2, grid.HitShip(0, 0))      
        
def main():
    unittest.main()

if __name__ == '__main__':
    main()
    
    