import csv
"""PlayerAction reads a CSV file of moves in and plays the game."""

class PlayerAction:
    
    def __init__(self):
        self.turn = 1 # Either 1 or 2, tells us whos turn it is 
    
    def ReadMoves(self, filename, grid1, grid2):
        with open(filename, newline='') as csvfile:
            csvread = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in csvread:
                
                # Check the length of the row
                if not len(row) == 2:
                    raise Exception("Invalid move input - must be a pair of coordinates")
                    
                # Check the moves are specified by integers 
                try:
                    x = int(row[0])
                except ValueError:
                    raise Exception("Move input is not an integer")
                try:
                    y = int(row[1])
                except ValueError:
                    raise Exception("Move input is not an integer")
                    
                self.MakeMove(grid1, grid2, x, y)

    def MakeMove(self,  grid1, grid2, x,  y):
        """Players take alternative turns shooting each other. 
        Print a congratulations message and end the program if
        all of one players ships are sunk."""
        if self.turn == 1:
            status = grid2.Shoot(x,y)
            if status == 3:
                print("Woop woop - go player 1")
                self.turn = 0
                return
            self.turn = 2
        elif self.turn == 2:
            status = grid1.Shoot(x,y)
            if status == 3:
                print("Woop woop - nice work Mr Marc")
                self.turn = 0
                return
            self.turn = 1
        # If someone has won, stop taking turns
        else:
            pass
