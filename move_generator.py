import itertools as it
import random

#open output file
outfile=open('Moves.csv','w')

A=range(10)

P1=list(it.product(A,repeat=2))
P2=list(it.product(A,repeat=2))

#random.shuffle(P1)
#random.shuffle(P2)

for i in range(len(P1)):
    outfile.write(str(P1[i][0])+','+str(P1[i][1])+'\n')
    outfile.write(str(P2[i][0])+','+str(P2[i][1])+'\n')
    
outfile.close()

print("Done!")




