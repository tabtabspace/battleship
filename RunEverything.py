# The basic flow:
# Import lots of stuff
# Load fleet data
# Place 1st players ships
# Place 2nd players ships
# Shoot it out

# PS: We dont use this code any more

import Fleet as flt
import Grid as grd
import PlayerAction as pa

fleet_filename = 'Fleet.csv'
fleet = flt.Fleet()
fleet.ReadCSV(fleet_filename)

grid1 = grd.Grid()
filename = 'Layout.csv'
grid1.ReadShipPositions(filename, fleet)

grid2 = grd.Grid()
filename = 'Layout.csv'
grid2.ReadShipPositions(filename, fleet)

#ReadShipPositions(self, filename, fleet)
player = pa.PlayerAction()
filename = 'Moves.csv'
player.ReadMoves(filename, grid1, grid2)

grid1.printing()
grid2.printing()

grid1.PlotGrid()
grid2.PlotGrid()