"""Ship class is used to hold the placement information about a 
single ship."""

class Ship:
    
    def __init__(self, x, y, shiptype, orientation):
        # Check the position of the ship makes sense
        try:        
            self.x = int(x)
            self.y = int(y)
        except ValueError:
            raise Exception("Move input is not an integer")
        if self.x < 0 or self.y < 0:
            raise Exception("You put a stupid ship position in") 
            
        self.shiptype = shiptype
        
        # Check orientation is allowed
        if orientation == 'H' or orientation == 'V':
            self.orientation = orientation
        else:
            raise Exception("Someone put a stupid orientation in") 