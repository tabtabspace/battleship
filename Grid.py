# Assume numpy is imported
import numpy as np
import csv
import Ship as sh
import matplotlib.pyplot

class Grid:
    
    grid_dimensions = (10,10) # The dimension of the battleship board, assume this is fixed at 10 x 10.
    
    def __init__(self):
        self.information = np.zeros(self.grid_dimensions) # This is a matrix the size of grid_dimensions that keeps track of the information each player has (hit, miss, or unknown for each space)
        self.positions = -np.ones(self.grid_dimensions) # This is a matrix the size of grid_dimensions that keeps track of the position of each ship by their .id (self._id)
        self.shipname = [] # Keeps track of the ship type, e.g. 'Carrier'
        self.shiphealth = [] # Keeps track of how many times each ship has been hit. 
        self._id = 0 # Private variable to keep track of how many ships have been created.
    
    def ReadShipPositions(self, filename, fleet):
        with open(filename, newline='') as csvfile:
            csvread = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in csvread:
                # Most of the error checking is in the Ship module.
                if not len(row) == 4:
                    raise Exception("The input length is NOT 4. But it should be.")
                
                # Split the row into each variable
                x = row[0]
                y = row[1]
                shiptype = row[2]
                orientation = row[3]
                assert(orientation=="V" or orientation=="H"), "Invalid orientation for "+str(shiptype)
                ship = sh.Ship(x,y,shiptype,orientation)
                self.Placeship(ship, fleet)
            
        
    def Placeship(self, ship, fleet):
        # Find index for fleet entry of particular ship
        index = [count for count in range(0,len(fleet.name)) if fleet.name[count] == ship.shiptype]
        if not len(index) == 1:
            raise Exception("Hey you, you are trying to place a ship type that doesn't exist")
        index = index[0] # This is to convert it from a list to a number. Apologies for the bad programming... 
        ship_length = fleet.length[index]

        """For each position the ship is trying to be placed on,
        if a ship is already there, throw an error,
        Otherwise, store the ships identification number in positions"""
        for count in range(0,ship_length):
            if (self.grid_dimensions[0] <= ship.x or self.grid_dimensions[1] <= ship.y or ship.x < 0 or ship.y < 0):
                raise Exception("Ship "+str(self._id)+" is off the grid")
            if self.positions[ship.x, ship.y] < 0:
                self.positions[ship.x, ship.y] = self._id
            else:
                raise Exception("Ships "+str(self._id)+" & "+str(int(self.positions[ship.x, ship.y]))+" overlap")
                
            assert(ship.orientation == 'H' or ship.orientation == 'V'), "Orientation is either H or V, why isn't it?"
            if ship.orientation == 'V':
                ship.x = ship.x + 1
            else:
                ship.y = ship.y + 1
        
        self.shipname.append(ship.shiptype)
        self.shiphealth.append(fleet.length[index])
        self._id = self._id + 1
        
    """Someone shoots at x,y. Check for a hit and return
    -1 - The person has already shot here (why are they shooting here again?)
    0 - Miss
    1 - Hit, no sink
    2 - Hit, sink
    3 - Hit, sink all"""
    def Shoot(self, x, y):
        status = -1
        assert(x >= 0 and y >= 0 and x < self.positions.shape[0] and y < self.positions.shape[1]), "Shoot at the grid, idiot"
        if not self.information[x, y] == 0:
            print('Skip the turn cuz you already shot there (why?)')
            status = -1
            return status
        # Check the space is occupied
        if self.positions[x, y] != -1:
            if self.information[x, y] == 0:
                status = self.HitShip(x, y)
        # If space is unoccupied, return information that it is a "miss"
        else:
            self.information[x,y] = 2
            status = 0
        return status        
       
    # If you hit the ship, reduce its health by one, and provide information about
    # whether it sunk
    def HitShip(self, x, y):
        hit_index = int(self.positions[x, y])
        self.shiphealth[hit_index] = self.shiphealth[hit_index] - 1
        self.information[x,y] = 1
        if self.shiphealth[hit_index] == 0:
            if sum(self.shiphealth) == 0:
                status = 3
            else:
                status = 2
        else:
            status = 1
        return status
            
    # I have no idea what the point of this function is. 
    def printing(self):
        print(self.information)  
        
    # Plot the information matrix to show hits (pink), misses (yellow), and unknown (blue)
    # This is outside the program requirements. Programmed to assist in debugging and because
    # of personal interest.
    def PlotGrid(self):
        normalize = matplotlib.colors.Normalize(vmin=0.,vmax=2.)
        matplotlib.pyplot.imshow(self.information,cmap='plasma', norm=normalize, interpolation='none')
        matplotlib.pyplot.show()